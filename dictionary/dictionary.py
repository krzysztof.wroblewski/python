"""
Program pozwalający na zbudowanie własnego słownika definicji.
1. dodawnie definicji do słownika
2. wyszukiwanie istniejących definicji
3. usuwanie wybranych definicji
4. zapisanie słownika do pliku
"""
import json

slowniczek = {}

def dodaj_haslo(slownik):
    print("Dodaj nowe hasło do słownika")
    key = input("Podaj klucz: ")
    definition = input("Podaj definicję: ")
    slownik[key] = definition
    print()

def wyszukaj_definicje(slownik):
    search = input("Wpisz czukane hasło: ")
    for haslo in slownik:
        if haslo == search:
            print(slownik[search], "\n")
        else:
            print("Brak takiej definicji w bazie \n")

def usun_wpis(slownik):
    print("Który wpis chcesz usunąć?")
    wpis = input()
    slownik.pop(wpis)
    print()

def zapisz_plik(slownik):
    json_object = json.dumps(slownik, indent = 4)
    with open("sample.json", "w") as outfile:
        outfile.write(json_object)

while (True):
    print("Wybierz opcję:")
    print("1. Dodawanie definicji do słownika")
    print("2. Wyszukanie definicji w słowniku")
    print("3. Usuwanie wpisów")
    print("4. Zapisanie słownika do pliku")
    print("0. Wyjście z programu")
    x = int(input())
    if (x == 1):
        dodaj_haslo(slowniczek)
    elif (x == 2):
        wyszukaj_definicje(slowniczek)
    elif (x == 3):
        usun_wpis(slowniczek)
    elif (x == 4):
        zapisz_plik(slowniczek)
    elif (x == 0):
        break
