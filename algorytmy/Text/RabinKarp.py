def generate_hash(text, pattern):

    len_text = len(text)
    len_pattern = len(pattern)

    ascii_text = [ord(i) for i in text]
    ascii_pattern = [ord(i) for i in pattern]

    hash_pattern = sum(ascii_pattern)

    len_hash_text = len_text - len_pattern + 1
    hash_text = [0] * len_hash_text

    for i in range(0, len_hash_text):
        if i == 0:
            hash_text[i] = sum(ascii_text[:len_pattern])
        else:
            hash_text[i] = (hash_text[i-1] - ascii_text[i-1] + ascii_text[i+len_pattern-1])

    return [hash_text, hash_pattern]    

def search_rabin_karp(text, pattern):

    hash_text, hash_pattern = generate_hash(text, pattern)

    len_text = len(text)
    len_pattern = len(pattern)

    result_list = []

    for i in range(len(hash_text)):

        if hash_text[i] == hash_pattern:
            matched_chars_count = 0
            for j in range(len_pattern):
                if pattern[j] == text[i+j]:
                    matched_chars_count += 1
                else:
                    break
            
            if matched_chars_count == len_pattern:
                result_list.append(i)
                print(f"Found at index {i}")
    
    return result_list