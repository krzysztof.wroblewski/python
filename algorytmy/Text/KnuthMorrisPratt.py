def generate_prefix(pattern):
    pat_len = len(pattern)
    pat_fun = [0] * pat_len
    pref_len = 0

    if pat_len <= 1:
        return pat_fun

    pat_fun[0] = 0
    i = 1

    while 1 < pat_len:
        if pattern[i] == pattern[pref_len]:
            pref_len += 1
            pat_fun[i] = pref_len
            i += 1

        else:
            if pref_len != 0:
                pref_len = pat_fun[pref_len-1]

            else:
                pat_fun[i] = 0
                i += 1
    return pat_fun

def search_pattern(pattern, text):
    pat_len = len(pattern)
    txt_len = len(text)
    pat_fun = generate_prefix(pattern)

    p = 0 # index pointing to pattern
    t = 0 # index pointing to text

    while t < txt_len:
        if pattern[p] == text[t]:
            t += 1
            p += 1
            if p == pat_len:
                print("Found pattern at index", t-p)
                p = pat_fun[p-1]

        else:
            if p != 0:
                p = pat_fun[p-1]
            else:
                t +=1