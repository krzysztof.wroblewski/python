def linear_search(number, array):
    index = 0
    is_found = False

    while not is_found and index < len(array):
        if array[index] == number:
            is_found = True
        else: 
            index += 1 

    return index if index < len(array) else None
