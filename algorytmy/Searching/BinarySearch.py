from tracemalloc import start


def binary_search(value, array):
    start_index = 0
    end_index = len(array)-1
    is_found = False

    while start_index <= end_index and not is_found:
        middle_index = (start_index + end_index) // 2 

        if array[middle_index] == value:
            is_found = True
        else:
            if value < array[middle_index]:
                end_index = middle_index - 1
            else:
                start_index = middle_index + 1

    if is_found:
        return middle_index
    else:
        return None