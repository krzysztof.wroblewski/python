def insert_sort(arr):
    for sort_border in range(1, len(arr)):
        index = sort_border - 1
        value = arr[index+1]

        while arr[index] > value and index >= 0:
            arr[index+1] = arr[index]
            index = index - 1

        arr[index+1] = value
    return arr