def merge_sort(arr):
    sorted_list = []
    middle_index = len(arr)//2

    if len(arr) <= 1:
        sorted_list = arr
    else:
        list_left = merge_sort(arr[middle_index:])
        list_right = merge_sort(arr[:middle_index])

        index_left = index_right = 0
        
        while index_left < len(list_left) and index_right < len(list_right):
            if list_left[index_left] < list_right[index_right]:
                sorted_list.append(list_left[index_left])
                index_left += 1
            else: 
                sorted_list.append(list_right[index_right])
                index_right += 1
        sorted_list.extend(list_left[index_left:])
        sorted_list.extend(list_right[index_right:])
    return sorted_list