def divide(list, start, end):

    i = start
    border_value = list[end]

    for j in range(start, end):
        if list[j] <= border_value:
            list[i], list[j] = list[j], list[i]
            i += 1

    list[i], list[end] = list[end], list[i]
    return i

def quick_sort(list, start, end):

    if start < end:
        border_index = divide(list, start, end)
        quick_sort(list, start, border_index-1)
        quick_sort(list, border_index+1, end)