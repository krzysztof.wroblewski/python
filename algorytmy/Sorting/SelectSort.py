def select_sort(arr):
    for index in range(len(arr)):
        min_index = index
        for i in range(index+1, len(arr)):
            if arr[i] < arr[min_index]:
                min_index = i
    
        arr[index], arr[min_index] = arr[min_index], arr[index] 

    return arr