from turtle import Turtle, Screen, colormode
import random
import colorgram

timmy = Turtle()
timmy.shape("turtle")
timmy.hideturtle()
timmy.pensize(5)
timmy.speed("fastest")
colormode(255)


def get_color_palette(image, number):
    """
    Extracts color palette from image of choice and converts values to rgb tuple
    """
    colors = colorgram.extract(image, number)
    color_palette = []
    for color in colors:
        r = color.rgb.r
        g = color.rgb.g
        b = color.rgb.b
        new_color = (r, g, b)
        color_palette.append(new_color)
    return color_palette


# Prepare the turtle, get him to starting position and penup() to stop drawing lines
timmy.penup()
timmy.setheading(225)
timmy.forward(250)
timmy.setheading(0)
# Prepare color list
color_list = get_color_palette("image.jpg", 10)
print(color_list)
# This loop is for mooving the turtle in lines and drop dots
for line in range(0, 10):
    # Inner loop moves turtle in line horizontally
    for column in range(0, 10):
        timmy.forward(50)
        timmy.dot(20, random.choice(color_list))
    # This part moves turtle all the  way back and up to next line
    timmy.setheading(180)
    timmy.forward(500)
    timmy.setheading(90)
    timmy.forward(50)
    timmy.setheading(0)

screen = Screen()
screen.exitonclick()
