from turtle import Turtle
ALIGNMENT = "center"
FONT = ("Noto Mono", 11, "bold")


class Scoreboard(Turtle):
    """This is a scoreboard class"""

    def __init__(self):
        super().__init__()
        self.total_score = 0
        with open("high_score.txt") as file:
            self.high_score = int(file.read())
        self.penup()
        self.hideturtle()
        self.speed("fastest")
        self.goto(0, 280)
        self.color("white")
        self.update_scoreboard()


    def update_scoreboard(self):
        self.clear()
        self.write(f"Score: {self.total_score} High Score: {self.high_score}",
                   align=ALIGNMENT,
                   font=FONT
                   )

    def increase_score(self):
        self.total_score += 1
        self.update_scoreboard()

    # def game_over(self):
    #     self.goto(0, 0)
    #     self.write("GAME OVER",
    #                align=ALIGNMENT,
    #                font=FONT)

    def reset(self):
        if self.total_score > self.high_score:
            self.high_score = self.total_score
        self.total_score = 0
        self.update_scoreboard()
        self.save_high_score()

    def save_high_score(self):
        with open("high_score.txt", mode="w") as file:
            file.write(str(self.high_score))


