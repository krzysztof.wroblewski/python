import requests
from bs4 import BeautifulSoup

URL = "https://www.billboard.com/charts/hot-100/"

date_to_jump = input("Do którego roku chesz się przenieść w czasie? Podaj datę w tym formacie: YYYY-MM-DD \n")
full_url = URL + date_to_jump

webpage = requests.get(full_url)
soup = BeautifulSoup(webpage.text, "html.parser")

songs_name = soup.select(selector="li h3", class_="c-title")
songs_list = []
 
for song in songs_name:
    text = song.getText().strip()
    songs_list.append(text)
 
print(songs_list)