from bs4 import BeautifulSoup
import requests

response = requests.get("https://news.ycombinator.com/")

soup = BeautifulSoup(response.text, "html.parser")

articles = soup.find_all(name="a", class_="titlelink")
article_texts = []
article_links= []

for article in articles:
    article_texts.append(article.getText())
    article_links.append(article.get("href"))

article_upvotes = [int(score.getText().split()[0]) for score in soup.find_all(name="span", class_="score")]

i = article_upvotes.index(max(article_upvotes))
print(article_upvotes[i])
print(article_texts[i])
print(article_links[i])