import requests
from bs4 import BeautifulSoup

URL = "https://web.archive.org/web/20200518073855/https://www.empireonline.com/movies/features/best-movies-2/"

response = requests.get(URL)
soup = BeautifulSoup(response.text, "html.parser")

headings = soup.find_all(name="h3", class_="title")
titles = []
for heading in headings:
    titles.append(heading.getText())

titles.reverse()
with open("movies.txt", "w") as file:
    for title in titles:
        file.write(title + "\n")




