from bs4 import BeautifulSoup
#alternative parser 
#import lxml

with open("website.html") as file:
    page_content = file.read()

soup = BeautifulSoup(page_content, "html.parser")

all_anchor_tags = soup.find_all(name="a")
print(all_anchor_tags)

for tag in all_anchor_tags:
   # print(tag.getText())
   print(tag.get("href"))

heading = soup.find(id="name")
print(heading)

section_heading = soup.find(name="h3", class_="heading")
print(section_heading)

company_url = soup.select_one(selector="p a")
print(company_url)

# id wybieramy w selektorze jako #id
# klasę wybieramy jako .klasa