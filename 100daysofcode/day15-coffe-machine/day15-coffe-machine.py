from resources import *


def prompt():
    """Asks for user input"""
    choice = input("What would you like? (espresso/latte/cappuccino): ")
    if choice == "off":
        return choice
    elif choice == "report":
        return choice
    else:
        coffee = MENU[choice]
        return coffee


def print_report(money):
    """Checks current resource state and prints it on the screen"""
    resource_list = list(resources.keys())
    for i in range(0,3):
        key = resource_list[i]
        print(str((key.title())), ": ", resources[key], "ml")
    print(f"Money: {money}")


def check_resources(coffee):
    """Checks if there is enough resources and returns 0 if true"""
    ingredient_list = coffee["ingredients"]
    if resources["water"] < ingredient_list["water"]:
        print("Sorry, not enough water")
        return 1
    elif resources["milk"] < ingredient_list["milk"]:
        print("Sorry, not enough milk")
        return 1
    elif resources["coffee"] < ingredient_list["coffee"]:
        print("Sorry, not enough coffee")
        return 1
    else:
        return 0


def process_coins(coffee):
    """Calculates the monetary value of inserted coins"""
    cost = coffee["cost"]
    print(f"Your coffee cost is {cost}")
    print("Quarters = $0.25, dimes = $0.10, nickles = $0.05, pennies = $0.01")
    quarters = int(input("How many quarters: "))
    dimes = int(input("How many dimes: "))
    nickles = int(input("How many nickles: "))
    pennies = int(input("How many pennies: "))
    value = quarters * 0.25 + dimes * 0.10 + nickles * 0.05 + pennies * 0.01
    return value


def check_transaction(coffee):
    """Checks if user inserted enough money"""
    cost = coffee["cost"]
    money_inserted = process_coins(coffee)
    if cost > money_inserted:
        print("Sorry, not enough money. Money refunded.")
        return 0
    else:
        change = "{:.2f}".format(money_inserted - cost)
        print(f"Here is {change} dollars in change")
        return cost


def make_coffee(coffee):
    ingredients = coffee["ingredients"]
    for resource in resources:
        resources[resource] -= ingredients[resource]
    print("Here is your coffee")


#Declare variables
power_status = True
money = 0

while power_status:
    choice = prompt()
    if choice == "off":
        power_status = False
    elif choice == "report":
        print_report(money)
    else:
        resource_status = check_resources(choice)
        if resource_status == 0:
            transaction_status = check_transaction(choice)
            if transaction_status > 0:
                make_coffee(choice)
                money += transaction_status
