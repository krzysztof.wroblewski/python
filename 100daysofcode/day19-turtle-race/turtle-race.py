from turtle import Turtle, Screen
import random

screen = Screen()
screen.setup(width=500, height=400)
user_bet = screen.textinput(title="Make your bet", prompt="Which turtle will win the race? Choose a color: ")
colors = ["red", "orange", "yellow", "green", "blue", "purple"]

turtles = list()


def create_turtle(i):
    new_turtle = Turtle(shape="turtle")
    new_turtle.color(colors[i])
    new_turtle.penup()
    return new_turtle


y_coordinate = -100
for i in range(0, 6):
    turtles.append(create_turtle(i))
    turtles[i].goto(x=-220, y=y_coordinate+40*i)


win_condition = False
while not win_condition:
    turtle_to_move = random.choice(turtles)
    how_far = random.randint(0,10)
    turtle_to_move.forward(how_far)
    if turtle_to_move.xcor() >= 250:
        win_condition = True
        winning_color = turtle_to_move.pencolor()
        if user_bet == winning_color:
            print(f"You win the bet! The {winning_color} turtle was the winner.")
        else:
            print(f"You lose the bet! The {winning_color} turtle was the winner.")


screen.exitonclick()