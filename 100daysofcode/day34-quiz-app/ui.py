from threading import Thread
from tkinter import * 
from quiz_brain import QuizBrain

THEME_COLOR = "#375362"

class QuizInterface:

    def __init__(self, quiz_brain: QuizBrain):
        self.quiz = quiz_brain
        self.window = Tk()
        self.window.title("Quiz")
        self.window.config(padx=20, pady=20, background=THEME_COLOR)

        self.label_score = Label(text=f"Score: {self.quiz.score}", foreground="white", background=THEME_COLOR)
        self.label_score.grid(row=0, column=1)

        self.canvas = Canvas(width=300, height=250, bg="white")
        self.question_text = self.canvas.create_text(
            150,
            125, 
            text="Some Question Text", 
            fill=THEME_COLOR,
            font=("Arial", 20, "italic"),
            width=280
            )
        self.canvas.grid(row=1, column=0, columnspan=2, pady=50)


        image_true = PhotoImage(file="images/true.png")
        image_false = PhotoImage(file="images/false.png")

        self.true_button = Button(image=image_true, highlightthickness=0, command=self.verify_answer_true)
        self.false_button = Button(image=image_false, highlightthickness=0, command=self.verify_answer_false)

        self.true_button.grid(row=2, column=0)
        self.false_button.grid(row=2, column=1)

        self.get_next_question()

        self.window.mainloop()


    def get_next_question(self):
        self.canvas.config(bg="white")
        if self.quiz.still_has_questions():
            q_text = self.quiz.next_question()
            self.canvas.itemconfig(self.question_text, text=q_text)
            self.label_score.config(text=f"Score: {self.quiz.score}")
        else:
            self.canvas.itemconfig(self.question_text, text="You've reached the end of the quiz")
            self.true_button.config(state="disabled")
            self.false_button.config(state="disabled")

    
    def verify_answer_true(self):
        self.give_feedback(self.quiz.check_answer("True"))


    def verify_answer_false(self):
        self.give_feedback(self.quiz.check_answer("False"))


    def give_feedback(self, is_right: bool):
        if is_right:
            self.canvas.config(bg="green")
        else:
            self.canvas.config(bg="red")
        
        self.window.after(1000, self.get_next_question)