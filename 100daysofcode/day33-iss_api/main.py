from tkinter.tix import Tree
from pytz import HOUR
import requests
import datetime

MY_LAT = 51.246452
MY_LNG = 22.568445

def check_position():
    response = requests.get(url="http://api.open-notify.org/iss-now.json")
    #raise exception based on status code
    response.raise_for_status()
    data = response.json()

    longitude = float(data["iss_position"]["longitude"])
    latitude = float(data["iss_position"]["latitude"])

    if MY_LAT - 5 <= latitude <= MY_LAT +5 and MY_LNG - 5 <= longitude <= MY_LNG + 5:
            return True
    return False


def check_sunset():
    parameters = {
        "lat": MY_LAT,
        "lng": MY_LNG,
        "formatted": 0,
    }

    response_sun = requests.get(url="https://api.sunrise-sunset.org/json", params=parameters)
    response_sun.raise_for_status()
    data_sun = response_sun.json()
    sunrise = int(data_sun["results"]["sunrise"].split("T")[1].split(":")[0])
    sunset = int(data_sun["results"]["sunset"].split("T")[1].split(":")[0])

    time_now = datetime.now().hour

    if time_now >= sunset or time_now <= sunrise:
        return True

    return False

if check_position() and check_sunset():
    print("ISS is overhead")
else:
    print("ISS is not visible right now")

