#from future.moves import tkinter
from tkinter import *

window = Tk()
window.title("Mile to Km Converter")
#window.minsize(width=500, height=300)
window.config(padx=20, pady=20)

def button_clicked():
    result = int(input.get()) * 1.6
    label_result.config(text=result)

input = Entry(width=10)
input.grid(column=1, row=0)

label_miles = Label(text="Miles")
label_miles.grid(column=2, row=0)

label_is_equal = Label(text="is equal to")
label_is_equal.grid(column=0, row=1)

label_result = Label(text="0")
label_result.grid(column=1, row=1)

label_km = Label(text="Km")
label_km.grid(column=2, row=1)

button = Button(text="Calculate", command=button_clicked)
button.grid(column=1, row=3)





window.mainloop()
