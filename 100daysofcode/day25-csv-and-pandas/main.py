# with open("weather_data.csv") as data_file:
#     data = data_file.readlines()
#     print(data)

# import csv

# with open("weather_data.csv") as data_file:
#     data = csv.reader(data_file)
#     temperatures = []
#     for row in data:
#         if row[1] != "temp":
#             temperatures.append(row[1])
#             print(int(row[1]))
#
# import pandas
#
# data = pandas.read_csv("weather_data.csv")
# # print(data["temp"])
#
# data_dict = data.to_dict()
# print(data_dict)
#
# temp_list = data["temp"].to_list()
# print(temp_list)
#
# avg = 0
# for temp in temp_list:
#     avg += temp
# avg = avg / len(temp_list)
# print("Average temp is: ", avg)
#
# avg = sum(temp_list) / len(temp_list)
# print("Average temp is: ", avg)
#
# avg = data["temp"].mean()
# print("Average temp is: ", avg)
#
# max = data["temp"].max()
# print("Max temp is: ", max)
#
# max = data.temp.max()
# print("Max temp id: ", max)
#
# row = data[data.day == "Monday"]
# print(row)
#
# row = data[data.temp == data.temp.max()]
# print(row)
#
# # dataframe from scratch
# data_dict = {
#     "students": ["Amy", "James", "Angela"],
#     "scores": [76, 56, 65]
# }
# data = pandas.DataFrame(data_dict)
# print(data)
# data.to_csv("new_data.csv")

import pandas

data = pandas.read_csv("2018_Central_Park_Squirrel_Census_-_Squirrel_Data.csv")


def count_squirrels(fur_colour):
    sqrl_count = len(data[data["Primary Fur Color"] == fur_colour])
    return sqrl_count


grey = count_squirrels("Gray")
red = count_squirrels("Cinnamon")
black = count_squirrels("Black")


data_dict = {
    "Fur Color": ["grey", "red", "black"],
    "Count": [grey, red, black]
}
data = pandas.DataFrame(data_dict)
print(data)
data.to_csv("squirrel_data.csv")



