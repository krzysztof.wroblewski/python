from art import *
from game_data import data
import random
from clear import clear

#Randomly choose two items from game_data
first_item = random.choice(data)
second_item = random.choice(data)

#Prepare some variables
score = 0

def main(first_item, second_item, score):
    """Main function of this game"""
    #Print game screen and user dialogue
    clear()
    print(logo)
    print(f"Compare A: {first_item['name']}, {first_item['description']} from {first_item['country']}.")
    print(vs)
    print(f"Against B: {second_item['name']}, {second_item['description']} from {second_item['country']}.")
    answer = input("Who has more Instagram followers? Type 'a' or 'b': ")

    #Compare and find out if answer was correct
    followers = first_item['follower_count'] - second_item['follower_count']
    if followers == 0:
        print("They have the same number of followers. Let's try another one.")
        first_item = second_item
        second_item = random.choice(data)
        main(first_item, second_item, score)
    elif (followers > 0 and answer == 'a') or (followers < 0 and answer == 'b'):
        first_item = second_item
        second_item = random.choice(data)
        score += 1
        main(first_item, second_item, score)
    else:
        print(f"Wrong answer! You lose.You score was: {score}")

main(first_item, second_item, score)
