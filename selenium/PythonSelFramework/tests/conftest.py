import pytest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service

def pytest_addoption(parser):
    parser.addoption(
        "--browser", action="store", default="chrome", help="Allows to choose browser: chrome or firefox"
    )


@pytest.fixture(scope="class")
def setup(request):
    browser_name = request.config.getOption("browser")

    if browser_name == "chrome":
        service_obj = Service("/home/krzysztof/Repozytorium/python/selenium/chromedriver")

    elif browser_name == "fireofx":
        #pretend we are using gecko driver for firefox
        service_obj = Service("/home/krzysztof/Repozytorium/python/selenium/chromedriver")

    driver = webdriver.Chrome(service=service_obj)
    driver.get("https://rahulshettyacademy.com/angularpractice/")
    driver.maximize_window()
    
    request.cls.driver = driver
    yield
    driver.close()