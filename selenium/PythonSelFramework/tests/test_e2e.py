
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
import pytest
from utilities.BaseClass import BaseClass
from pageObjects.HomePage import HomePage
from pageObjects.CheckoutPage import CheckoutPage

#@pytest.mark.usefixtures("setup")
class TestOne(BaseClass):

    def test_e2e(self):
        self.driver.implicitly_wait(4)
       
        homePage = HomePage(self.driver)
        
        checkOutPage = homePage.shopItems()
        
        # self.driver.find_element(By.LINK_TEXT, "Shop").click()
        # driver.find_element(By.CSS_SELECTOR, "a[href*='shop']")
        # driver.find_element(By.XPATH, "a[contains(@href, 'shop')])")

        """
        button .card .card-footer .btn
        title .card .card-body .card-title
        """

        #cards = checkOutPage.getCards(self.driver)
        cards = checkOutPage.getCardTitles(self.driver)

        count = len(cards)
        assert count > 0

        #for item in cards:
        #    if item.find_element(By.CSS_SELECTOR, ".card-body .card-title").text == "Blackberry":
        #        item.find_element(By.CSS_SELECTOR, ".card-footer .btn").click()

        i = -1
        for card in cards:
            i = i + 1
            cardText = card.text
            print(cardText)
            if cardText == "Blackberry":
                checkOutPage.getCardFooter()[i].click()


        self.driver.find_element(By.CSS_SELECTOR, "a[class*='btn-primary']").click()
        checkOutPage.getCheckoutItems.click()
        self.driver.find_element(By.ID, "country").send_keys("poland")

        self.verifyLinkPresence("Poland")

        self.driver.find_element(By.LINK_TEXT, "Poland").click()
        self.driver.find_element(By.XPATH, "//div[@class='checkbox checkbox-primary']").click()
        self.driver.find_element(By.CSS_SELECTOR, "[type='submit']").click()
        successText = self.driver.find_element(By.CLASS_NAME, "alert-success").text

        assert "Success" in successText