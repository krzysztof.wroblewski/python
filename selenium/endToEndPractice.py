from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions

service_obj = Service("/home/krzysztof/Repozytorium/python/selenium/chromedriver")
driver = webdriver.Chrome(service=service_obj)
driver.get("https://rahulshettyacademy.com/angularpractice/")

driver.implicitly_wait(4)

driver.find_element(By.LINK_TEXT, "Shop").click()
# driver.find_element(By.CSS_SELECTOR, "a[href*='shop']")
# driver.find_element(By.XPATH, "a[contains(@href, 'shop')])")

"""
button .card .card-footer .btn
title .card .card-body .card-title
"""
cards = driver.find_elements(By.CSS_SELECTOR, ".card")

count = len(cards)
assert count > 0

for item in cards:
    if item.find_element(By.CSS_SELECTOR, ".card-body .card-title").text == "Blackberry":
        item.find_element(By.CSS_SELECTOR, ".card-footer .btn").click()

driver.find_element(By.CSS_SELECTOR, "a[class*='btn-primary']").click()
driver.find_element(By.CSS_SELECTOR, ".btn-success").click()
driver.find_element(By.ID, "country").send_keys("poland")

#EXPLICIT WAIT
wait = WebDriverWait(driver, 10)
wait.until(expected_conditions.presence_of_element_located((By.LINK_TEXT, "Poland")))
driver.find_element(By.LINK_TEXT, "Poland").click()
driver.find_element(By.XPATH, "//div[@class='checkbox checkbox-primary']").click()
driver.find_element(By.CSS_SELECTOR, "[type='submit']").click()
successText = driver.find_element(By.CLASS_NAME, "alert-success").text

assert "Success" in successText