from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

service_obj = Service("/home/krzysztof/Repozytorium/python/selenium/chromedriver")
driver = webdriver.Chrome(service=service_obj)
driver.get("https://rahulshettyacademy.com/angularpractice/")

# ID, Xpath, CSSSelector, Classname, name, linkText
driver.find_element(By.NAME, "email").send_keys("kwroblewski@mail.com")
driver.find_element(By.ID, "exampleInputPassword1").send_keys("password")
driver.find_element(By.ID, "exampleCheck1").click()

# Pole tekstwe - css selector
driver.find_element(By.CSS_SELECTOR, "input[name='name'").send_keys("Krzysztof")

# Static Dropdown
dropdown = Select(driver.find_element(By.ID, "exampleFormControlSelect1"))
dropdown.select_by_index(1)
dropdown.select_by_visible_text("male")

# Zatwierdzamy formularz przyciskiem - xpath
driver.find_element(By.XPATH, "//input[@type='submit']").click()

# Sprawdzamy komunikat potwierdzający
result = driver.find_element(By.CLASS_NAME,"alert-success").text
print(result)

# Sprawdzamy czyt komunikat zwrotny zawiera słowo Success
assert "Success" in result

driver.close()