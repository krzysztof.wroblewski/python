# Każdy plik Pytest musi się zaczynać od test_
# metody testowe muszą się zaczynać od test_
# kod musi być zawarty w metodach testowych 
# można odpalić przez test runner w IDE albo z terminala wpisując polecenie py.test w odpowiednim katalogu
# parametry dla py.test to:
# -v verbose
# -s success test cases info
# -k klucz którego szukamy w nazwach testów - te zostaną wykonane 
# można odpalić wybrany test przez py.test filename.py
#
# moża grupować testy za pomocą tzn. marks @pytest.mark.smoke - analogicznie do grup w testng
# takie testy odpalamy py.test -m smoke
# pytest ma duzo predefiniowanych tagów jak np. skip - pomija test
# mark.xfail - nie reportuje tego testu 
#
# fixture- to co się dzieje przed testem jak @before w testng
# i w tym fixture mogę też określić działania po wykonainiu testu - yield
# jeśli fixture dotyczy więcej niż jednego pliku możewmy je umieścic w conftest.py
# i współdzielić między testami w róznych plikach
# fixture dodajemy do testu przekazując w parametrze nazwe fixture 'setup'
# możemy też opakować wszystkie testy w klasę i na tym poziomie podpiąć fixture
# dodajemy anotację @pytest.mark.usefixtures("setup")
# przekazywanie parametrów możemy zrobić przez przekazanie parametrów jako tuple w return 
# 
# raporty html generujemy za pomocą pakietu pytest-html
# wywołujemy py.test --html=report.html czyli z dodatkowym parametrem



import pytest

@pytest.mark.usefixtures("setup")
class TestExample:

    def test_firstProgram(self):
        print("Hello")


    @pytest.mark.smoke
    @pytest.mark.skip
    def test_secondTest(self):
        msg = "Hi"
        assert msg == "Hello"

    def test_fixtureDemo2 (self):
        print("I will execute steps in fixtureDemo 2")

